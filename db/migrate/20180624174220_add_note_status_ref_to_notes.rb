class AddNoteStatusRefToNotes < ActiveRecord::Migration[5.1]
  def change
    add_reference :notes, :note_status, foreign_key: true
  end
end