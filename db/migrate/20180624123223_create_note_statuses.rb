class CreateNoteStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :note_statuses do |t|
      t.text :status

      t.timestamps
    end
  end
end
