User.create!(name:  "Example User",
    email: "example@example.org",
    password:              "password",
    password_confirmation: "password",
    admin: true)

99.times do |n|
name  = Faker::Name.name
email = "example-#{n+1}@example.org"
password = "password"
User.create!(name:  name,
      email: email,
      password:              password,
      password_confirmation: password)
end

@note_ntatus = (['draft' , 'ready for export' , 'exported' ])
@note_ntatus.each do |status|
  NoteStatus.create!(status: status)
end

users = User.order(:created_at).take(6)
50.times do
  title = Faker::Lorem.sentence(1)
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.notes.create!(title: title, content: content, note_status_id: NoteStatus.all.first.id) }
end

