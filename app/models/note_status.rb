class NoteStatus < ApplicationRecord
    has_many :notes
    validates :status, presence: true, length: { maximum: 80 }
end
