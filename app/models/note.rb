class Note < ApplicationRecord
  belongs_to :user
  belongs_to :note_status
  #default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :note_status_id, presence: true
  validates :title, presence: true, length: { maximum: 80 }
  validates :content, presence: true, length: { maximum: 140 }

  def self.search(search)
    if search
      where("title LIKE :search OR content LIKE :search", search: "%#{search}%")
    else
      all
    end
  end

  def self.filter(filter_value)
    if filter_value
      where("note_status_id = ?", filter_value)
    else
      all
    end
  end

end
