// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).ready((function() {
    $(".aside .current, .aside .pagination a").on("click", function() {
      $.getScript(this.href);
      return false;
    });

    $("#notes_search #search").keyup(function() {
      $.get($("#notes_search").attr("action"), $("#notes_search").serialize(), null, "script");
      return false;
    });

    $(function() {
      $("#filter_by_status, select, #note_note_status_id").on("change", function() {
        //var data = $("#filter_by_status, select, #note_note_status_id").val();
        var data = 3;
        console.log(data);
          $.ajax({
              url:  $("#notes_search").attr("action"),
              type: "GET",
              data: { filter_status: data }
          });
          return false;
      });
    });
    console.log($("#filter_by_status, select, #note_note_status_id").val());
  }));

