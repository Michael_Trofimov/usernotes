class StaticPagesController < ApplicationController

  def home
    if logged_in?
      @note = current_user.notes.build
      @note_items = current_user.get_notes_user.filter(params[:filter_status]).search(params[:search]).order(sort_column + " " + sort_direction).paginate(page: params[:page], :per_page => 3)
      #respond_to do |format|
        #format.html { render 'home'}
        #format.js { render :partial => @note_items, :formats => [:html]} 
        #format.js {render :layout=>false}
      #end
    end
  end

  def help
  end

  def about
  end

  def contact
  end

end
