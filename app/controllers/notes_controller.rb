class NotesController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy, :edit]
    before_action :correct_user,   only: [:destroy, :edit]

    def create
      @note = current_user.notes.build(note_params)
      if @note.save
        flash[:success] = "Note created!"
        redirect_to root_url
      else
        @note_items = []
        render 'static_pages/home'
      end
    end

    def edit
      render 'notes/edit'
    end

    def update
      @note = Note.find(params[:id])
    if @note.update_attributes(note_params)
      flash[:success] = "Note updated"
      redirect_to request.referrer || root_url
    else
      render 'edit'
    end
  end
    
    def destroy
      @note.destroy
      flash[:success] = "Note deleted"
      redirect_to request.referrer || root_url
    end
    
      private
    
        def note_params
          params.require(:note).permit(:title, :content, :note_status_id)
        end

        def correct_user
          @note = current_user.notes.find_by(id: params[:id])
          redirect_to root_url if @note.nil?
        end

end
