require 'test_helper'

class NotesInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "note interface" do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    # Invalid submission
    assert_no_difference 'Note.count' do
      post notes_path, params: { note: { content: "" } }
    end
    assert_no_difference 'Note.count' do
      post notes_path, params: { note: { title: "" } }
    end
    assert_select 'div#error_explanation'
    # Valid submission
    title = "Valid note title"
    content = "Valid note content"
    note_status_id = note_statuses(:draft).id
    assert_difference 'Note.count', 1 do
      post notes_path, params: { note: { title: title, content: content, note_status_id: note_status_id} }
    end
    assert_redirected_to root_url
    follow_redirect!
    assert_match title, response.body
    assert_match content, response.body
    # Delete post
    assert_select 'a', text: 'delete'
    first_note = @user.notes.paginate(page: 1).first
    assert_difference 'Note.count', -1 do
      delete note_path(first_note)
    end
    # Visit different user (no delete links)
    get user_path(users(:inna))
    assert_select 'a', text: ' delete', count: 0
    assert_select 'a', text: ' edit', count: 0
  end
end
