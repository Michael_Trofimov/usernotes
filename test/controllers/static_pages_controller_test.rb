require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "User Notes"
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | User Notes"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | User Notes"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | User Notes"
  end


  test "Notes list at home display" do       
    log_in_as(users(:michael))
    assert is_logged_in?
    get root_path
    assert_template 'static_pages/home'
    assert_select 'h1', text: @user.name
    assert_match @user.notes.count.to_s, response.body
    assert_select 'div.pagination'
    @user.get_notes_user.order("created_at" + " " + "desc").paginate(page: 1, :per_page => 3).each do |note|
      assert_match note.title, response.body
      assert_match note.content, response.body
    end
  end

end