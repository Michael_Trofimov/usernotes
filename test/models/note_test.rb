require 'test_helper'

class NoteTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @note_statuses = note_statuses(:draft)
    @note = @user.notes.build(title: "Test title", content: "Test message", note_status_id: @note_statuses.id)

  end

  test "should be valid" do
    assert @note.valid?
  end

  test "user id should be present" do
    @note.user_id = nil
    assert_not @note.valid?
  end

  test "content should be present" do
    @note.content = "   "
    assert_not @note.valid?
  end

  test "content should be at most 140 characters" do
    @note.content = "a" * 141
    assert_not @note.valid?
  end

  test "title should be present" do
    @note.title = "   "
    assert_not @note.valid?
  end

  test "title should be at most 80 characters" do
    @note.title = "a" * 81
    assert_not @note.valid?
  end

  #test "order should be most recent first" do
    #assert_equal notes(:most_recent), Note.first
  #end
end
